export enum EditSwitch {
    addStone = 0  ,
    removeStone = 1 ,
    changeBorderRight = 2 ,
    changeBorderBottom = 3 ,
    exit = 4 ,
}

