import {Component} from '@angular/core';
import {Robot} from './robot';
import {Direction} from './direction.enum';
import {EditSwitch} from './edit.enum';
import {Field} from './field';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    robot = new Robot();
    Direction = Direction;
    editorSwitch = false;
    editSwitch = EditSwitch;
    editStatus = undefined;
    gameStatus = false;
    onOffGameStatus = 'Start Game';
    fieldThere = false;
    isButtonEdit = false;
    isButtonCreate = false;
    isButtonMove = false;
    numberOfCols: number;
    numberOfRows: number;
    isButtonTurn = false;


    constructor() {
        this.numberOfCols = 10;
        this.numberOfRows = 10;
    }

    createField() {
        this.fieldThere = true;
        this.robot.field = new Field(this.numberOfCols, this.numberOfRows);
    }


    modalBtnEditSwitch() {
        this.isButtonEdit = !this.isButtonEdit;
        return this.isButtonEdit;
    }

    modalBtnCreateSwitch() {
        this.isButtonCreate = !this.isButtonCreate;
        return this.isButtonCreate;
    }

    modalBtnMoveSwitch() {
        this.isButtonMove = !this.isButtonMove;
        return this.isButtonMove;
    }
    modalBtnTurnSwitch() {
        this.isButtonTurn = !this.isButtonTurn;
        return this.isButtonTurn;
    }

    getContentArray(content) {
        return Array(content).fill(1);
    }

    async putDownandMove() {
        this.robot.putDown();
        await this.robot.moveForward();
    }

    reloadGame() {
        window.location.reload();
    }

    async pickUpAndMove() {
        await this.robot.moveForward();
        this.robot.pickUp();
    }

    async theGame() {
        if (this.gameStatus === false) {
            this.gameStatus = true;
        } else if (this.gameStatus === true) {
            this.reloadGame();
        }
        for (let i = 0; i < 5; i++) {
                await this.robot.moveForward();

        }
        await this.robot.turnLeft();
    }

    changeStart() {
        this.onOffGameStatus = 'Restart Game';
    }

    choseOption(x, y) {
        switch (this.editStatus) {
            case this.editSwitch.addStone:
                this.robot.getCell(x, y).addContent();
                break;
            case this.editSwitch.removeStone:
                this.robot.getCell(x, y).removeContent();
                break;
            case this.editSwitch.changeBorderRight:
                this.robot.getCell(x, y).borderRight = !this.robot.getCell(x, y).borderRight;
                break;
            case this.editSwitch.changeBorderBottom:
                this.robot.getCell(x, y).borderBottom = !this.robot.getCell(x, y).borderBottom;
                break;
        }
    }

    increaseXValue() {
        this.numberOfCols++;
    }

    decreaseXValue() {
        this.numberOfCols--;
    }

    increaseYValue() {
        this.numberOfRows++;
    }

    decreaseYValue() {
        this.numberOfRows--;
    }

    forwardFreeMove() {
        if (this.robot.isForwardFree()) {
            this.robot.moveForwardInstant();
        }
        return false;
    }
}
