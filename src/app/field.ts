import {Cell} from './cell';

export class Field {

  field: Array<Array<Cell>> = new Array();

  constructor(rows: number
    , cols: number) {

    for (let i = 0; i < cols; i++) {
      let colArray = new Array();
      this.field.push(colArray);
      for (let j = 0; j < rows; j++) {
        colArray.push(new Cell());
      }
    }
  }
}
