import {Direction} from './direction.enum';
import {Field} from './field';
import {ApplicationRef} from '@angular/core';
import {letProto} from 'rxjs/operator/let';
import {Cell} from './cell';

export class Robot {
    positionX: number = 4;
    positionY: number = 5;
    direction: Direction = Direction.UP;
    field: Field;


    sleep(milliseconds) {
        return new Promise(resolve => setTimeout(resolve, milliseconds));
    }


    async turnLeft() {
        await this.sleep(1000);
        this.direction++;
        if (this.direction === 4) {
            this.direction = 0;
        }
    }

    turnLeftInstant() {
        this.direction++;
        if (this.direction === 4) {
            this.direction = 0;
        }
    }

    async turnRight() {
        this.turnLeft();
        this.turnLeft();
        await this.turnLeft();
    }

    turnRightInstant() {
        this.turnLeftInstant();
        this.turnLeftInstant();
        this.turnLeftInstant();
    }

    async turnBack() {
        this.turnLeft();
        await this.turnLeft();
    }

    turnBackInstant() {
        this.turnLeftInstant();
        this.turnLeftInstant();
    }

    isForwardFree() {
        switch (this.direction) {
            case Direction.UP:
                return this.isTopFree();
            case Direction.DOWN:
                return this.isBottomFree();
            case Direction.LEFT:
                return this.isLeftFree();
            case Direction.RIGHT:
                return this.isRightFree();
        }

    }

    private isBottomFree() {

        let cell = this.getCell(this.positionX, this.positionY + 1);
        if (!cell) {
            return false;
        }
        return !this.getCurrentCell().borderBottom;

    }

    private isTopFree() {
        let cell = this.getCell(this.positionX, this.positionY - 1);
        if (!cell) {
            return false;
        }
        return !cell.borderBottom;
    }

    private isLeftFree() {
        let cell = this.getCell(this.positionX - 1, this.positionY);
        if (!cell) {
            return false;
        }
        return !cell.borderRight;
    }

    private isRightFree() {
        let cell = this.getCell(this.positionX + 1, this.positionY);
        if (!cell) {
            return false;
        }
        return !this.getCurrentCell().borderRight;

    }

    pickUp() {
        this.getCurrentCell().removeContent();
    }

    putDown() {
        this.getCurrentCell().addContent();
    }

    isCellFilled() {
        this.getCurrentCell().isCellFilled();
    }

    async moveForward() {
        await this.sleep(1000);
        this.moveForwardInstant();
    }

    moveForwardInstant() {
        if (!this.isForwardFree()) {
            throw 'you hit the wall';
        }
        switch (this.direction) {
            case Direction.UP:
                this.positionY--;
                break;
            case Direction.DOWN:
                this.positionY++;
                break;
            case Direction.LEFT:
                this.positionX--;
                break;
            case Direction.RIGHT:
                this.positionX++;
                break;
        }
        if (!this.getCurrentCell()) {
            throw 'you hit the limit of your field';
        }

    }

    getCell(x, y) {
        if (x < 0 || y < 0 || y >= this.field.field.length || x >= this.field.field[0]) {
            return null;
        }

        return this.field.field[y][x];
    }

    getCurrentCell() {
        return this.getCell(this.positionX, this.positionY);
    }
}
