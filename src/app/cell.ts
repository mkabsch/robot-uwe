export class Cell {

    borderBottom: boolean = false;
    borderRight: boolean = false;

    content: number = 0;

    addContent() {
        this.content++;
    }

    removeContent() {
        if (this.content === 0) {
            throw 'there is nothing to remove.';
        }
        this.content--;

    }

    isCellFilled() {
        return this.content > 0;
    }
}
