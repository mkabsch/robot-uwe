import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunktionenComponent } from './funktionen.component';

describe('FunktionenComponent', () => {
  let component: FunktionenComponent;
  let fixture: ComponentFixture<FunktionenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunktionenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunktionenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
