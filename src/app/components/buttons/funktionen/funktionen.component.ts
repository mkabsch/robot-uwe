import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-funktionen',
    templateUrl: './funktionen.component.html',
    styleUrls: ['./funktionen.component.css']
})
export class FunktionenComponent implements OnInit {

isButtonEdit = false;

isButtonCreate = false;

// modalBtnEdit = document.getElementById("modal-btn-edit");
  // modalEdit = document.querySelector(".modalEdit");
  // closeEdit = document.querySelector(".closeEdit");


  /*modalBtnEdit () {
    modalEdit.style.display = "block"
    this.isButtonEdit = true;
    return this.isButtonEdit;
  };*/

  modalBtnEditSwitch () {
    this.isButtonEdit = !this.isButtonEdit;
    return this.isButtonEdit;
  };

  modalBtnCreateSwitch () {
      this.isButtonCreate = !this.isButtonCreate;
      return this.isButtonCreate;
  };

/*
  closeEdit.onclick = function () {
    modalEdit.style.display = "none"
  };

  modalBtnCreate = document.getElementById("modal-btn-create");
  modalCreate = document.querySelector(".modalCreate");
  closeCreate = document.querySelector(".closeCreate");

  modalBtnCreate.onclick = function () {
    modalCreate.style.display = "block"
  };
  closeCreate.onclick = function () {
    modalCreate.style.display = "none"
  };

*/

  constructor() {
    }

    ngOnInit() {
    }

}
